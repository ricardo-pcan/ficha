import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import classNames from 'classnames';

export class Discover extends Component {
  render() {
    return (
      <div>
        <div className="paragraph-style">
          <img className="img-discover" src="../../assets/images/temporary/work-sheets/statues.png" />
          <p>La intención de esta experiencia consiste en resolver con álgebra un enigma vinculado a un hecho histórico y artístico que forma parte de la cultura universal.</p>
          <span>
            <p>El emperador chino Qin Shi Huang estaba tan obsesionado con la idea de morir que prohibía a sus allegados pronunciar la palabra muerte
              y al mismo tiempo orden. La creación de un ejército que lo acompañara en su viaje al más allá; se hicieron entonces miles de ﬁguras en
              terracota, de tamaño natural y todos con distinto rostro, incluso pertenecientes a diferentes etnias, de manera que no hay dos figuras
              iguales. Hace poco trompo se encontró un pergamino casi intacto en el que se lee lo que alguno de sus consejeros le dijo al emperador:<br/>
              t00 que no lo saben.</p>
            <p>El emperador Qin, irritado, le dijo:</p>
            <p>- Te pregunto cuántos guerreros tengo, no que saben o ignoran.</p>
            <p>- Ya se lo he dicho, emperador -dijo contundente su consejero.</p>
          </span>
        </div>
        <div className="grey-square">
          <h3>Información adicional:</h3>
          <p>En el año 221 a. C. se usó la forma huangdi para referirse a este primer emperador chino (huang significa "rey dios" y Di, "rey sabio") de la dinastía Qin, que terminó en 1911. Finalmente, dicho término se abrevió como "Huang" o "Di", perdiendo cada una de las partes su significado original.</p>
          <h3>Vínculo con:</h3>
          <p>Historia Universal, Geografía, Formación Cívica y Ética.</p>
          <h3>Emoción:</h3>
          <p>Pregunte lo siguiente:<br/>¿Puedes distinguir alguna emoción al leer este texto y ver la imagen? ¿Podrías describirla? Ayude a los alumnos a hacer contacto con sus emociones y expresarlas por escrito.</p>
          <p>Este texto y su imagen pretenden generar asombro, admiración o sorpresa, pero es posible que cada estudiante reconozca una emoción distinta dependiendo de sus aprendizajes y experiencias previas.</p>
        </div>
        <div className="reflection">
          <h1>
            <img src="/assets/images/temporary/sheet1.png" />
            <span>¿Qué emociones sintieron<br/>tus alumnos?</span>
          </h1>
          <table className="table-valuations table-padding">
            <thead>
              <tr>
                <th>Emoción</th>
                <th>Promedio en el grupo</th>
                <th>Detalle</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Alegría</td>
                <td>
                  <div className="percentage">
                    <div>6 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '20%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Miedo</td>
                <td>
                  <div className="percentage">
                    <div>2 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '6.6%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Tristeza</td>
                <td>
                  <div className="percentage">
                    <div>8 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '26.6%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Sorpresa</td>
                <td>
                  <div className="percentage">
                    <div>3 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '10%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Disgusto</td>
                <td>
                  <div className="percentage">
                    <div>6 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '20%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Otro</td>
                <td>
                  <div className="percentage">
                    <div>5 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '16.6%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}


export class Think extends Component {
  render() {
    return (
      <div>
        <div className="paragraph-style">
          <p>-¿Cómo se podría resolver el "enigma de los Guerreros de terracota"?, ¿qué estrategia utilizarías?</p>
          <p>-Platica con tu pareja o con tu equipo y pregúntales qué estrategia eligieron.</p>
          <p>-¿Te sirve comparar otras estrategias con las tuyas?, ¿por qué?</p>
          <p>-Elijan la que consideren más útil.</p>
          <p>-Una manera de resolver  este enigma  es plantear una ecuación que nos ayude a calcular el número de guerreros. De las siguientes ecuaciones, elige la que equivalga a lo que el consejero le dio a entender al emperador:</p>
          <img src="/assets/images/temporary/work-sheets/equation1.png" />
          <p>-Complementen la historia: investiguen quiénes fueron los Guerreros de terracota, dónde se encuentran y por qué se les considera patrimonio de la humanidad.</p>
        </div>
        <div className="grey-square">
          <h3 className="title-margin">Idea</h3>
          <p><strong>a.</strong> Respuesta correcta.</p>
          <p><strong>b.</strong> El consejero dio a entender al emperador que había x guerreros, donde <span><img src="/assets/images/temporary/work-sheets/equation2.png" /></span><br/>Esta opción la escogerán aquellos que ignoren la información sobre los 1,100 guerreros que se incluyen en el texto. Hay que notar que esta ecuación no tiene solución</p>
          <p><strong>c.</strong> El consejero dio a entender al emperador que había x guerreros, donde x = 100 + 1000.<br/>Esta opción la elegirán los que no tomen en cuenta la información que se da sobre las fracciones del total.</p>
          <p>El consejero dio a entender al emperador que había x guerreros, donde <span><img src="/assets/images/temporary/work-sheets/equation3.png" /></span></p>
        </div>
      </div>
    );
  }
}

export class Create extends Component {
  constructor(props) {
    super(props);
    this.goActivity = this.goActivity.bind(this);
  }
  goActivity() {
    browserHistory.push('/teachers/terracota');
  }
  render() {
    return (
      <div>
        <div className="paragraph-style">
          <p>-A partir de sus resultados, elaboren un modelo que represente la cantidad de guerreros que conformaban el ejército; para ello, elijan una de las siguientes opciones para completar el enunciado que aparece a continuación:</p>
          <p>El consejero dio a entender al emperador que había<br/>a. 4400 guerreros.<br/>b. 1100 guerreros.<br/>c. 2200 guerreros.<br/>d. 220 guerreros.</p>
          <p>-¿Cuá de las opciones es correcta?<br/>-¿Crees haber resuelto el enigma de los Guerreros de terracota?<br/>-¿Podrían aplicar lo que han hecho a otros casos que les interesen?</p>
        </div>
        <div className="grey-square">
          <h3 className="title-margin">Crea</h3>
          <p>Este es un espacio para que los alumnos elijan libremente, y por concenso entre ellos, las herramientas y materiales que utilizarán para exponer sus ideas y compartirlas en su portafolio de experiencias.</p>
        </div>
        <div className="text-right">
          <RaisedButton
            target="_blank"
            label="Enviar a Classroom"
            icon={<i className="fa fa-users" style={{color: '#ffffff'}}></i>}
            style={{marginTop: '20px'}}
            backgroundColor="#008a50"
            labelColor="#ffffff"
            />
        </div>
        <div>
          <div className="rectangle-style bar-green" id="crea" style={{backgroundColor: '#764AFF'}}>
            <div className="bar-txt">
              Actividad
            </div>
            <div className="toggle-info">
              <i
              className={classNames('fa', 'bar-icon', 'fa-caret-down')}
              aria-hidden="true"
              >
              </i>
            </div>
          </div>
          <div className="student-main-activity">
            <div className="row">
              <div className="col-sm-5">
                <CardMedia>
                  <iframe width="100%" height="230" src="https://www.youtube.com/embed/NaFd_tFbpXE" frameBorder="0" allowFullScreen></iframe>
                </CardMedia>
              </div>
              <div className="col-sm-7">
                <div className="activity-description">
                  <h2>Documental Los Soldados de Terracota</h2>
                  <h3>Descripción:</h3>
                  <p>Te invitamos a conocer más sobre los guerreros de terracota. Después del ver el video (investigación) escribe un ensayo donde expliques dónde se encuentran y porqué se consideran patrimonio de la humanidad. </p>
                  <h4>Más información:</h4>
                  <p>Conoce más sobre estos personajes en estas ligas: </p>
                  <p><a href="https://www.youtube.com/watch?v=NaFd_tFbpXE">https://www.youtube.com/watch?v=NaFd_tFbpXE</a></p>
                  <p><a href="https://www.youtube.com/watch?v=RsUE-ZtcUFg">https://www.youtube.com/watch?v=RsUE-ZtcUFg</a></p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-5">
                <p>Los Soldados de Terracota.</p>
              </div>
              <div className="col-sm-7">
                <div className="activity-description">
                  <div className="row">
                    <div className="col-sm-6">
                      <RaisedButton
                        label="Agrega más información"
                        backgroundColor={'##9E9E9E'}
                        labelColor={'#fff'}
                        fullWidth={true}
                        labelStyle={{fontWeight: '400'}}
                      />
                    </div>
                    <div className="col-sm-6">
                      <RaisedButton
                        label="Ir a la actividad"
                        backgroundColor={'#283593'}
                        labelColor={'#fff'}
                        fullWidth={true}
                        labelStyle={{fontWeight: '400'}}
                        onClick={this.goActivity}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="reflection">
          <h1>
            <img src="/assets/images/temporary/sheet1.png" />
            <span>¿Qué emociones sintieron<br/>tus alumnos?</span>
          </h1>
          <table className="table-valuations table-padding">
            <thead>
              <tr>
                <th>Emoción</th>
                <th>Promedio en el grupo</th>
                <th>Detalle</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Alegría</td>
                <td>
                  <div className="percentage">
                    <div>10 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '33.3%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Miedo</td>
                <td>
                  <div className="percentage">
                    <div>2 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '6.6%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Tristeza</td>
                <td>
                  <div className="percentage">
                    <div>3 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '10%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Sorpresa</td>
                <td>
                  <div className="percentage">
                    <div>3 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '10%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Disgusto</td>
                <td>
                  <div className="percentage">
                    <div>3 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '10%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Otro</td>
                <td>
                  <div className="percentage">
                    <div>9 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '30%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export class Habilities extends Component {
  render() {
    return (
      <div className="reflection">
        <table className="table-valuations table-padding">
          <thead>
            <tr>
              <th>Emoción</th>
              <th>Promedio en el grupo</th>
              <th>Detalle</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Comunicación</td>
              <td>
                <div className="percentage">
                  <div>22 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '73.3%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Colaboración</td>
              <td>
                <div className="percentage">
                  <div>23 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '76.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Creatividad</td>
              <td>
                <div className="percentage">
                  <div>8 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '26.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Resolución de<br/>Problemas</td>
              <td>
                <div className="percentage">
                  <div>26 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '86.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Manejo de<br/>información</td>
              <td>
                <div className="percentage">
                  <div>11 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '36.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Uso de medios</td>
              <td>
                <div className="percentage">
                  <div>8 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '26.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Manejo de<br/>tecnología</td>
              <td>
                <div className="percentage">
                  <div>15 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '50%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Curiosidad</td>
              <td>
                <div className="percentage">
                  <div>17 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '56.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Iniciativa</td>
              <td>
                <div className="percentage">
                  <div>8 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '26.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Persistencia</td>
              <td>
                <div className="percentage">
                  <div>25 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '83.3%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export class Enhance extends Component {
  render() {
    return (
      <div>
        <div>
          <h4>MED RELACIONADOS</h4>
        </div>  
        <table className="table-calendar table-border">
          <thead>
            <tr>
              <th className="th-border">Fichas relacionadas</th>
              <th>Planeaciones relacionadas</th>
              <th>Meds relacionas</th>
              <th>Evaluaciones sugeridas disponibles</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <div className="meds-set">
                  <div>
                    <div className="img-relative">
                      <img src="/assets/images/temporary/calendar/cal1.png" />
                      <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                    </div>
                  </div>
                  <div>
                    <p className="title">Lengua Castellana y poemas del s...</p>
                    <p>Duración 2 días</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal6.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Conocer una cancion</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal10.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Día Mundial de la Población</p>
                  </div>
                </div>
              </td>
              <td>
                <a href='#'>4 reactivos</a>
              </td>
            </tr>
            <tr>
              <td>
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal2.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p className="title">Albúm de los recuerdos de la..</p>
                    <p>Duración 4 días</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal7.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Las diferentes manifestaciones culturales</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal11.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>¿En qué se parece Shakespeare y</p>
                  </div>
                </div>
              </td>
              <td>
                <a href='#'>5 reactivos</a>
              </td>
            </tr>
            <tr>
              <td>
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal3.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p className="title">La biblioteca y yo</p>
                    <p>Duración 2 dias</p>
                  </div>
                </div>
                <div className="meds-set" style={{marginTop: '20px'}}>
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal4.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p className="title">Información para un futuro</p>
                    <p>Duración 2 dias</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal8.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Comunica ideas y sentimientos</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal12.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Verificador de Ortagrafía en Línea</p>
                  </div>
                </div>
              </td>
              <td>
                <a href='#'>4 reactivos</a>
              </td>
            </tr>
            <tr>
              <td>
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal5.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p className="title">¿Qué es la jerarquía?</p>
                    <p>Duración 1 día</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal9.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Conocer los datos biográficos</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal13.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Lengua Castellana y literatura</p>
                  </div>
                </div>
              </td>
              <td>
                <a href='#'>6 reactivos</a>
              </td>
            </tr>
          </tbody>
        </table>
        <div>
          <h4>MED RELACIONADOS</h4>
        </div>
        <table className="table-med-rel">
          <thead>
            <tr>
              <th className="th-border">Título</th>
              <th>Descripción</th>
              <th >Ver</th>
              <th>Agregar</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal10.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Día Mundial de la Población</p>
                  </div>
                </div>
              </td>
              <td>
                <p>Libro que contiene ejemplos de rubricas orientadas a evaluar diferentes productos.</p>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal11.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>¿En qué se parecen Shakespeare y Cervantes? O... El Día Mundial del</p>
                  </div>
                </div>
              </td>
              <td>
                <p>Se recomienda al docente que lea el documento para utilizar las rúbricas que contiene o generar las propias.</p>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal12.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Verificardor de Ortografía en Línea</p>
                  </div>
                </div>
              </td>
              <td>
                <p>Documento de Administración Federal de Servicios Educativos que presenta 20 juegos en que los estudiantes se diviertan mientras desarrollan.</p>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal13.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Lengua Castellana y literatura</p>
                  </div>
                </div>
              </td>
              <td>
                <p>Este MED ayudará a orientar a los educadores y docentes de educación temprana para que ayuden a los niños a aprender de acuerdo a su propio estilo.</p>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
          </tbody>
        </table>
        <div>
          <h4>REACTIVOS RELACIONADOS</h4>
        </div>
        <table className="table-rea-rel">
          <thead>
            <tr>
              <th className="th-border">Reactivo</th>
              <th>Respuestas</th>
              <th>Agregar</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <p>Al efectuar la suma 5/3 + 8/6 se obtiene como resultado.</p>
              </td>
              <td>
                <p className="correct"><span>a)</span> 4/8</p>
                <p><span>b)</span> 12/24</p>
                <p><span>c)</span> 13/9</p>
                <p><span>d)</span> 3/24</p>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>
              <td>
                <p>Don Guillermo compró un automóvil que cuneta $93,580. Si dio un enganche de 15% ¿Cuánto le falta por pagar?.</p>
              </td>
              <td>
                <p><span>a)</span> 13-913</p>
                <p><span>b)</span> 14.038</p>
                <p className="correct"><span>c)</span> 79.543</p>
                <p><span>d)</span> 79.667</p>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>  
              <td>
                <p>En una fábrica se empacan latas de atún. Si en 100 cajas caben 1500 latas, ¿Cuántas se podrán acomodar en 350 cajas?.</p>
              </td>
              <td>
                <p className="correct"><span>a)</span>4/8</p>
                <p><span>b)</span>   12/24</p>
                <p><span>c)</span>   13/9</p>
                <p><span>d)</span>   3/24</p>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>  
          </tbody>
        </table>
        <div>
          <h4>PLANEACIONES RELACIONADOS</h4>
        </div>
        <table className="table-pla-rel">
          <thead>
            <tr>
              <th>PLAN</th>
              <th>Aprendizaje</th>
              <th>Aprendizaje</th>
              <th>Ver</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="title">161</td>
              <td>Resolver un problema de forma autónoma</td>
              <td>Resuelve problema que implican el cálculo mental o escrito de productos digitos</td>
              <td><i className="fa fa-eye" aria-hidden="true"></i></td>
            </tr>
            <tr>
              <td className="title">162</td>
              <td>Resolver un problema de forma autónoma</td>
              <td>Resuelve problema que implican el cálculo mental o escrito de productos digitos</td>
              <td><i className="fa fa-eye" aria-hidden="true"></i></td>
            </tr>
            <tr>
              <td className="title">163</td>
              <td>Uso de caminos cortos para multiplicar digitos por 10 o por sus múltiplos (20, 30, etcétera)</td>
              <td>Resuelve problema que implican el cálculo mental o escrito de productos digitos</td>
              <td><i className="fa fa-eye" aria-hidden="true"></i></td>
            </tr>
            <tr>
              <td className="title">164</td>
              <td>Resolver un problema de forma autónoma</td>
              <td>Resuelve problema que implican el cálculo mental o escrito de productos digitos</td>
              <td><i className="fa fa-eye" aria-hidden="true"></i></td>
            </tr>  
          </tbody>
        </table>
      </div>
    );
  }
}

export class SimpleVideo extends Component {
  render() {
    return (
      <div className="video-config">
        <iframe className="video-position" width="80%" height="500" src="https://www.youtube.com/embed/_mDlhV9fJWI" frameBorder="0" allowFullScreen></iframe>
      </div>
    );
  }
}