import React, {Component} from 'react';
import classNames from 'classnames';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Discover, Think, Create, Enhance, Habilities, SimpleVideo} from './sections';


import './style.scss';

export class Ficha extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: [false, false, false, false, false, false]
    };  
  }

  toggleOpen(index) {
    let newArray = this.state.open.slice(0);
    newArray[index] = !newArray[index];
    this.setState({
      open: newArray
    });
  }

  render() {

    return (
      <MuiThemeProvider>

      <div className="main-container">
        <h3 className="h3-title"><span>Bloque III Español / </span>Ejemplo de ficha</h3>
        <div className="line-wrapper line-width">
          <a href="#descubre">
            <div className="hexagon-wrapper">
              <img src="/assets/images/elementos/descubreA.png" />
            </div>
          </a>
          <a href="#idea">
            <div className="hexagon-wrapper">
              <img src="/assets/images/elementos/ideaA.png" />
            </div>
          </a>
          <a href="#crea">
            <div className="hexagon-wrapper">
              <img src="/assets/images/elementos/creaA.png" />
            </div>
          </a>
          <a href="#habilidades">
            <div className="hexagon-wrapper">
              <img src="/assets/images/elementos/habilidadesA.png" />
            </div>
          </a>
          <div className="img-column">
            <div>
              <a href="#enhance">
                <img src="/assets/images/icon/eye.png" />
              </a>
              <a href="#enhance">
                <img src="/assets/images/icon/play.png" />
              </a>
            </div>
          </div>
          <div className="img-column-2">  
            <div>
              <a href="#enhance">
                <img src="/assets/images/icon/pdf.png" />
              </a>
              <a href="#enhance">
                <img src="/assets/images/icon/plus.png" />
              </a>
            </div>
          </div>
        </div>
        <div className="div-border"></div>
        <div className="work-sheet-item">          
          <div className="rectangle-style bar-blue" id="descubre">
            <div className="bar-txt">
              Descubre
            </div>
            <div className="toggle-info" onClick={this.toggleOpen.bind(this, 0)}>
              <i
                className={classNames('fa', 'bar-icon', this.state.open[0]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true"
              >
              </i>
            </div>
          </div>
          {this.state.open[0]?<Discover />:null}
          
          <div>
            <div className="div-border"></div>
            <div className="rectangle-style bar-pink" id="idea">
              <div className="bar-txt">
                Idea
              </div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 1)}>
                <i
                className={classNames('fa', 'bar-icon', this.state.open[1]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true"
                >
                </i>
              </div>
            </div>
            {this.state.open[1]?<Think />:null}
            
            <div className="rectangle-style bar-green" id="crea">
              <div className="bar-txt">
                Crea
              </div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 2)}>
                <i
                className={classNames('fa', 'bar-icon', this.state.open[2]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true"
                >
                </i>
              </div>
            </div>
            {this.state.open[2]?<Create />:null}
            
            <div className="div-border"></div>
            <div className="rectangle-style bar-yellow" id="habilidades">
              <div className="bar-txt">
                Mis habilidades del SXII
              </div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 3)}>
                <i
                className={classNames('fa', 'bar-icon', this.state.open[3]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true"
                >
                </i>
              </div>
            </div>
            {this.state.open[3]?<Habilities />:null}
            
            <div className="rectangle-style bar-navi-blue" id="descubre">
              <img src="/assets/images/temporary/work-sheets/recomendaciones.png" />
              <div className="txt-basis" id="enhance">Enriquece tu clase</div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 4)}>
                <i
                className={classNames('fa', 'bar-icon', this.state.open[4]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true">
                </i>
              </div>
            </div>
            {this.state.open[4]?<Enhance />:null}
            
            <div className="div-border"></div>
            <div className="rectangle-style bar-grey">
              <div className="box-config">
                <i className="fa fa-lightbulb-o" aria-hidden="true"></i>
              </div>
              <div className="grey-basis">
                Visita al Mousoleo
              </div>
              <div className="toggle-info" onClick={this.toggleOpen.bind(this, 5)}>
                <i
                className={classNames('fa', 'bar-icon', this.state.open[5]?'fa-caret-up':'fa-caret-down')}
                aria-hidden="true"
                >
                </i>
              </div>
            </div>
            {this.state.open[5]?<SimpleVideo />:null}
            <div className="div-border"></div>
            <div className="stars-row">
              <h1>Calificación promedio de esta actividad</h1>
              <img src="/assets/images/temporary/work-sheets/starOn.png" />
              <img src="/assets/images/temporary/work-sheets/starOn.png" />
              <img src="/assets/images/temporary/work-sheets/starOn.png" />
              <img src="/assets/images/temporary/work-sheets/starOn.png" />
              <img src="/assets/images/temporary/work-sheets/starOff.png" />
            </div>
            <div className="div-border"></div>
          </div>
        </div>        
      </div>
      </MuiThemeProvider>
    );
  }
}

export default Ficha